public class Account {
    private Double balance;
    private String name;

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest(){
        this.balance = this.balance + (this.balance * .10);
    }
    


}